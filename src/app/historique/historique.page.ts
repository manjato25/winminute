import { Component, OnInit } from '@angular/core';
import { CollectService } from '../api/collect.service';
import Swal from 'sweetalert2';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { ModalController } from '@ionic/angular';
import { ModalPagePage } from '../modal-page/modal-page.page';

@Component({
  selector: 'app-historique',
  templateUrl: './historique.page.html',
  styleUrls: ['./historique.page.scss'],
})
export class HistoriquePage implements OnInit {
  dataHistorique: any;
  dataSearch = [];
  message: string;
  date: any;
  collectResult: any;
  result: boolean;
  collectPdv: any;
  searchBar: any;
  dateName: string;
  filter: boolean;
  dataReturned: any;
  dataPdv: any;
  urlSearch: any;
  dateSearch: string;
  searchPicker: string;
  dataAvalaible = false;

  constructor(
    private collectService: CollectService,
    private datePicker: DatePicker,
    public modalController: ModalController) { }
  historique() {
    this.collectService.getHistory().subscribe((data: any) => {
      this.dataHistorique = data
      this.dataAvalaible = true
    })
  }
  deleteFilter() {
    this.dateName = '';
    this.searchBar = '';
    this.dataPdv = '';
    this.date = '';
    this.historique();
    this.filter = false;
  }
  ionViewWillEnter() {
    this.searchBar = '';
    this.date = '';
}
  showDatepicker(){
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK,
      okText:"Valider",
      cancelText: 'Annuler',
      minDate: new Date('January 01, 19 00:00:00').valueOf()
    }).then(
      date => {
        const a = date.getFullYear() + "-" + (date.getMonth() + 1).toString().padStart(2, "0");
        this.dateName = date.getDate()+"/"+date.toLocaleString('default', { month: 'long' })+"/"+date.getFullYear();
        this.dateSearch = a;
        if (this.dataReturned !== undefined) {
          this.searchPicker = this.dateSearch.toString() + '&collect_date=' + this.dateSearch
        } else {
          this.searchPicker = this.dateSearch.toString()
        }
        this.collectService.searchCollect(this.searchPicker).subscribe((data: any) => {
          if (data.length === 0) {
            this.message = 'il n\'y a pas de résultats'
            Swal.fire({
              text: this.message,
              showConfirmButton: false,
              icon: 'error',
              timer: 2000,
              showClass: {
                popup: 'animated fadeInDown faster'
              },
              hideClass: {
                popup: 'animated fadeOutUp faster'
              }
            });
            this.filter = false; 
          } else {
            this.dataHistorique = data;
            this.filter = true;
          }
        })
      },
      err => {
        Swal.fire({
          text: err,
          showConfirmButton: false,
          icon: 'error',
          timer: 2000,
          showClass: {
            popup: 'animated fadeInDown faster'
          },
          hideClass: {
            popup: 'animated fadeOutUp faster'
          }
        });
      }
    );
  }

  async openModal(a) {
    const modal = await this.modalController.create({
      component: ModalPagePage,
      componentProps: {
        "value": a,
      }
    });
 
    modal.onDidDismiss().then((dataReturned) => {
      console.log(dataReturned);
      this.filter = true;
      if (dataReturned.data !== undefined && dataReturned.data !== null) {
        this.dataReturned = dataReturned.data;
        this.dataPdv = dataReturned.data;
        if (this.dateSearch !== undefined) {
          this.urlSearch = this.dataReturned.db7_retailer_id + '&collect_date=' + this.dateSearch
        } else {
          this.urlSearch = this.dataReturned.db7_retailer_id
        }
        this.collectService.getIdCollect(this.urlSearch).subscribe((data: any) => {
          this.dataHistorique = data;
        })
      }
    });
 
    return await modal.present();
  }
  ngOnInit() {
    this.historique();
  }
  searchCollect () {
    this.result = true;
    if (this.date !== null) {
      this.collectService.searchCollect(this.date.substring(0, 7)).subscribe((data:any) => {
        this.dataHistorique = data;
        this.collectService.setCollet(this.dataHistorique);
       if (this.dataHistorique === 0) {
          this.message = 'il n\'y a pas de résultats'
          Swal.fire({
            text: this.message,
            showConfirmButton: false,
            icon: 'error',
            timer: 2000, 
            showClass: {
              popup: 'animated fadeInDown faster'
            },
            hideClass: {
              popup: 'animated fadeOutUp faster'
            }
          });
        }
      })
    }
 
  }
}
