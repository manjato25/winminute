import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CampaignService } from '../api/campaign.service';

@Component({
  selector: 'app-questionnaire',
  templateUrl: './questionnaire.page.html',
  styleUrls: ['./questionnaire.page.scss'],
})
export class QuestionnairePage implements OnInit {
  img = 'assets/img/icon_app_listing.png'
  questionData: any;
  dataAvalaible = false;
  constructor(
    private activatedRoute: ActivatedRoute,
    private campaignService: CampaignService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: any) => {
      this.campaignService.camptaignQuestion(params.campaign_id, params.retailer_id).subscribe(data => {
        console.log(data);
        this.questionData = data.questionnaire
        this.dataAvalaible = true;
      },
      err => console.log(err))
        });
  }

}
