import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import * as sha1 from 'sha1';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  formLogin: FormGroup;
  img = 'assets/img/home-brand.png'
  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private router: Router
  ) { 
    this.createformLogin();
  }
  createformLogin() {
    this.formLogin = this.formBuilder.group({
      email: ['', Validators.compose([
        Validators.required,
        Validators.email
      ])],
      pw: ['', Validators.required],

    });
  }
  logForm() {
    const user = {
      login: this.formLogin.get('email').value,
      password: sha1(this.formLogin.get('pw').value),
     language: 'FR'
    };
    this.auth.signin(user).subscribe((resp:any) => {
       this.auth.storeUserData(resp.token);
      Swal.fire({
        text: 'Bienvenue',
        showConfirmButton: false,
        icon: 'success',
        timer: 2000,
        showClass: {
          popup: 'animated fadeInDown faster'
        },
        hideClass: {
          popup: 'animated fadeOutUp faster'
        }
      });
      this.auth.androidVersion().subscribe((data:any) => {
        if (data.status === 'ok') {
          Swal.fire({
            text: 'Bienvenue',
            showConfirmButton: false,
            icon: 'success',
            timer: 2000,
            showClass: {
              popup: 'animated fadeInDown faster'
            },
            hideClass: {
              popup: 'animated fadeOutUp faster'
            }
          });
          this.router.navigate(['/home'])
          location.reload();
        } else if (data.status === 'ko' && data.must === true) {
          Swal.fire({
            text: 'Une nouvelle mise a jour est disponible',
            showConfirmButton: false,
            icon: 'error',
            timer: 2000, 
            showClass: {
              popup: 'animated fadeInDown faster'
            },
            hideClass: {
              popup: 'animated fadeOutUp faster'
            }
          });
        } else if (data.status === 'ko' && data.must === false) {
          Swal.fire({
            text: "Il y a une nouvelle version disponible",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Mettre a jours',
            cancelButtonText: 'Plus tard',
          }).then((result) => {
            if (result.value) {
              Swal.fire(
                'Ok',
                'Redirection URL MAJ',
                'success'
              )
            }
          })
        }
      },
      err => {
        Swal.fire({
          text: err.error.message,
          showConfirmButton: false,
          icon: 'error',
          timer: 3000
        })
      });
    },
    err => {
      Swal.fire({
        text: err.error.message,
        showConfirmButton: false,
        icon: 'error',
        timer: 3000
      })
    });
  }
  ngOnInit() {
  }

}
