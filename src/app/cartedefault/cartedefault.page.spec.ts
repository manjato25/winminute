import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CartedefaultPage } from './cartedefault.page';

describe('CartedefaultPage', () => {
  let component: CartedefaultPage;
  let fixture: ComponentFixture<CartedefaultPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartedefaultPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CartedefaultPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
