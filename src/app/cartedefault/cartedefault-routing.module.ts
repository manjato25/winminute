import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CartedefaultPage } from './cartedefault.page';

const routes: Routes = [
  {
    path: '',
    component: CartedefaultPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CartedefaultPageRoutingModule {}
