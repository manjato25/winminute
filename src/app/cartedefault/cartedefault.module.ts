import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CartedefaultPageRoutingModule } from './cartedefault-routing.module';

import { CartedefaultPage } from './cartedefault.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CartedefaultPageRoutingModule
  ],
  declarations: [CartedefaultPage]
})
export class CartedefaultPageModule {}
