import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { UserService } from '../api/user.service';
import { AuthService } from '../services/auth.service';
import Swal from 'sweetalert2';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-mon-profil',
  templateUrl: './mon-profil.page.html',
  styleUrls: ['./mon-profil.page.scss'],
})
export class MonProfilPage implements OnInit {
  info: any;
  dataAvalaible = false;
  dateNaiss: string;
  pass = 'aaaaaa';
  sexes = [];
  prenom: any;
  nom: any;
  idSexe: any;
  adresse: any;
  zip: any;
  numero: any;
  formAdresse: FormGroup;
  csp =  [
    {
      nom: '--',
      value: '1'
    },
    {
      nom: 'Artisans-Commerçants-Chef d\'entreprise',
      value: '2'
    },
    {
      nom: 'Cadre et professions intellectuelles supérieures',
      value: '3'
    },
    {
      nom: 'Professions intermédiaires',
      value: '4'
    },
    {
      nom: 'Indépendant / freelance',
      value: '5'
    },
    {
      nom: 'Employés',
      value: '6'
    },  
    {
      nom: 'Ouvriers',
      value: '7'
    },
    {
      nom: 'En recherche d\'emploi',
      value: '8'
    },  
    {
      nom: 'Parent au foyer / Etudiant / Retraité',
      value: '9'
    },
  ];
  cspname: any;
  constructor(
    private userService: UserService,
    private authService: AuthService,
    private formBuilder: FormBuilder,) { 
      this.createformAdresse()
    }
    compareById(o1, o2) {
      return o1 === o2
    }
  ngOnInit() {
    this.profilUser();
  }
  profilUser() {
    this.userService.profil().subscribe((res: any) => {
      this.info = res;
      this.cspname = res.csp;
      this.prenom = res.db2_first_name;
      this.nom = res.db2_last_name;
      this.dateNaiss = res.db2_birthdate;
      this.adresse = res.db2_address;
      this.zip = res.db2_home_zip;
      this.numero = res.db2_mobile_phone;
      if (res.db2_gender === '1') {
        this.sexes = [
          {
            type: 'Homme',
            checked: true,
          },
          {
            type: 'Femme',
            checked: false,
          },
        ];
        this.idSexe = 1;
      } else {
        this.sexes = [
          {
            type: 'Homme',
            checked: false
          },
          {
            type: 'Femme',
            checked: true
          },
        ];
        this.idSexe = 2;
      }
      this.dataAvalaible = true
    })
  }
  formatDate(input) {
    var datePart = input.match(/\d+/g),
      year = datePart[0],
      month = datePart[1],
      day = datePart[2];
    return day + '/' + month + '/' + year;
  }
  logout() {
    this.authService.logout();
  }
  setSexe(a, i) {
   this.idSexe = i + 1;
    this.sexes.forEach((res: any) => {
      res.checked = false;
    });
    a.checked = true;
    
  }
  createformAdresse() {
    this.formAdresse = this.formBuilder.group({
      adresse: ['', Validators.compose([
        Validators.required,
      ])],
      zipCode: ['', Validators.compose([
        Validators.minLength(5),
        Validators.maxLength(5),
        Validators.pattern("^[0-9]*$"),
      ])],
      numero : ['', Validators.compose([
      ])],

    });
  }
  saveInfo(a, b ,c ) {
    const user = {
      db2_last_name: this.nom,
      db2_first_name: this.prenom,
      db2_gender: this.idSexe.toString(),
      db2_birthdate: this.dateNaiss,
      db2_address: a,
      db2_home_zip: b,
      db2_mobile_phone: c,
      csp: this.cspname
    };
    console.log(user);
     this.userService.editUser(user).subscribe((data: any) => {
      Swal.fire({
        text: data.message,
        showConfirmButton: false,
        icon: 'success',
        timer: 2000,
        showClass: {
          popup: 'animated fadeInDown faster'
        },
        hideClass: {
          popup: 'animated fadeOutUp faster'
        }
      });
      this.profilUser();
    }, err => {
      Swal.fire({
        text: err,
        showConfirmButton: false,
        icon: 'error',
        timer: 2000,
        showClass: {
          popup: 'animated fadeInDown faster'
        },
        hideClass: {
          popup: 'animated fadeOutUp faster'
        }
      });
    })
  }
}
