import { Component, OnInit } from '@angular/core';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import Swal from 'sweetalert2';
import { AuthService } from '../services/auth.service';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
@Component({
  selector: 'app-reglage',
  templateUrl: './reglage.page.html',
  styleUrls: ['./reglage.page.scss'],
})
export class ReglagePage implements OnInit {
  photoLibrary: any;
  gpsValue: boolean;
  cameraValue: boolean;
  fileValue: boolean;
  capturedSnapURL: string;
  constructor(
    private androidPermissions: AndroidPermissions,
    private locationAccuracy: LocationAccuracy,
    private authService: AuthService,
    private diagnostic: Diagnostic,
    private camera: Camera
  ) { }

  deleteAccount() {
  }

  /* GPS */
  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
      } else {
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
          .then(
            () => {
              this.askToTurnOnGPS();
            },
            error => {
              Swal.fire({
                icon: 'error',
                text: error,
              })
              this.gpsValue = false
            }
          );
      }
    });
  }
  askToTurnOnGPS() {
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => { 
      },
      error =>  {
        Swal.fire({
          icon: 'error',
          text: error,
        })
        this.gpsValue === false
      }
      
    );
  }
  /* END OF GPS */

  /* FICHIER */
filePerm() {
  this.androidPermissions.hasPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
  .then(status => {
    if (status.hasPermission) {
    } else {
      this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
      .then(status =>{
        alert(status)
      });
    }
  })
}
  /* END OF FICHIER */

  /* APN */
requestCamera() {
  this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
    result => alert('Has permission?'+ result.hasPermission),
    err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA)
  );
}
  /* END OF APN */
  
  /* PHOTOS GALLERY */
  requestGalleryPhoto() {
    this.photoLibrary.requestAuthorization({read:true,write:true})
  }
  /* END OF PHOTOS GALLERY */
  logout() {
    this.authService.logout();
  }
  changeGPS(e) {
   if (this.gpsValue === false) {
    this.diagnostic.switchToLocationSettings();
    this.gpsValue = true
   } else {
    this.checkGPSPermission()
   }
   
  }
  changefile(e) {
    if (this.fileValue === false) {
      this.gpsValue = true
     } else {
   this.filePerm()
     }
  }
  changeCamera(e) {
    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.GET_ACCOUNTS]).then(result => {
      this.checkCameraPermission()
    });
  }
  checkCameraPermission() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
      result => {
        alert(result.hasPermission)
        if (result.hasPermission) {
         this.cameraValue = true
        } else {
        this.cameraValue = false
        }
      },
      err => {
        Swal.fire({
          icon: 'error',
          text: err,
        })
      }
    );
  }
  checkGPSPermission() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      result => {
        if (result.hasPermission) {
          this.askToTurnOnGPS();
        } else {
          this.requestGPSPermission();
        }
      },
      err => {
        Swal.fire({
          icon: 'error',
          text: err,
        })
      }
    );
  }
  checkFilePermission(){
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(
      result => {
        if (result.hasPermission) {
         this.fileValue = true
        } else {
        this.fileValue = false
        }
      },
      err => {
        Swal.fire({
          icon: 'error',
          text: err,
        })
      }
    );
  }
  checkCamera() {
    let successCallback = (isAvailable) => {
      if (isAvailable === true) {
        this.cameraValue = true;
      } else {
        this.cameraValue = false
      }
     }
    let errorCallback = (e) => alert(e);
    this.diagnostic.isCameraAvailable().then(successCallback).catch(errorCallback);

  }
  checkLocation() {
    let successCallback = (isAvailable) => { 
      if (isAvailable === true) {
        this.gpsValue = true;
      } else {
        this.gpsValue = false
      }
    }
    let errorCallback = (e) => alert(e);
    this.diagnostic.isLocationAvailable().then(successCallback).catch(errorCallback);
  }
  checkExternalStorage() {
    let successCallback = (isAvailable) => {
      if (isAvailable === true) {
        this.fileValue = true;
      } else {
        this.fileValue = false
      }
    }
    let errorCallback = (e) => alert(e);
    this.diagnostic.isExternalStorageAuthorized().then(successCallback).catch(errorCallback);

  }
  ngOnInit() {
    this.checkLocation();
    this.checkExternalStorage();
    this.checkCamera();
  }

}
