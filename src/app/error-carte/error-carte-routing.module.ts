import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ErrorCartePage } from './error-carte.page';

const routes: Routes = [
  {
    path: '',
    component: ErrorCartePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ErrorCartePageRoutingModule {}
