import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ErrorCartePageRoutingModule } from './error-carte-routing.module';

import { ErrorCartePage } from './error-carte.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ErrorCartePageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [ErrorCartePage]
})
export class ErrorCartePageModule {}
