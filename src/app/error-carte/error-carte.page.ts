import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-error-carte',
  templateUrl: './error-carte.page.html',
  styleUrls: ['./error-carte.page.scss'],
})
export class ErrorCartePage implements OnInit {
  formCode: FormGroup;
  img = 'assets/img/icon_app_warning.png'
  constructor(
    private formBuilder: FormBuilder,
    private router: Router) {
      this.createformZip()
     }
  createformZip() {
    this.formCode = this.formBuilder.group({
      zipCode: ['', Validators.compose([
        Validators.minLength(5),
        Validators.maxLength(5),
        Validators.pattern("^[0-9]*$"),
      ])],
    });
  }
  setZipCode () {
    const codePostal = this.formCode.get('zipCode').value;
    this.router.navigateByUrl('carte-postal/'+ codePostal);
  }
  ngOnInit() {
  }

}
