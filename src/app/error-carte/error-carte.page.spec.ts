import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ErrorCartePage } from './error-carte.page';

describe('ErrorCartePage', () => {
  let component: ErrorCartePage;
  let fixture: ComponentFixture<ErrorCartePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorCartePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ErrorCartePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
