import { Component, OnInit } from '@angular/core';
import { UserService } from '../api/user.service';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{
  img = 'assets/img/home-brand.png'
  img2 = 'assets/img/cliquez-ici_FR.jpg'
  nom: any;
  dataAvalaible = false;
  constructor(
      private userService: UserService,
  ) {}
welc() {
  this.userService.userHome().subscribe((data:any) => {
    this.nom = data.db2_first_name;
    this.dataAvalaible = true;
  } )
}

  ngOnInit() {
    this.welc();
  }
}
