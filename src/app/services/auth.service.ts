import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Storage } from '@ionic/storage';
import { Platform } from '@ionic/angular';

const TOKEN_KEY = 'Token';
@Injectable({
  providedIn: 'root'
})

export class AuthService {
  baseUrl = environment.ws_url;
  token: any;
  authenticationState = new BehaviorSubject(false);
  constructor(
    private http: HttpClient,
    private storage: Storage,
    private plt: Platform) {
      this.plt.ready().then(() => {
        this.checkToken();
      });
     }

  signin(user): Observable<any> {
    return this.http.post(this.baseUrl + 'user/login', user);
  }
  androidVersion(): Observable<any> {
    return this.http.get(this.baseUrl + 'application/version?platform=android&version=1.0.0');
  }
  resetpw(email): Observable<any> {
    return this.http.post(this.baseUrl + 'user/request_password', email);
  }
  storeUserData(token) {
    this.storage.set('Token', token ).then(() => {
      this.authenticationState.next(true);
    });
  }
  public checkToken(){
     this.storage.get(TOKEN_KEY).then((res)=>{
      if (res) {
        this.setToken(res);
        this.authenticationState.next(true);
      }
    });
  }
  public getToken() {
    return this.token
  }
  setToken(token) {
    this.token = token;
  }
  isAuthenticated() {
    return this.authenticationState.value;
  }
  logout() {
    return this.storage.remove(TOKEN_KEY).then(() => {
      this.authenticationState.next(false);
    });
  }
}
