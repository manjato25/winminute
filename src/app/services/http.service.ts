import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class HhtpService {
  httpOptions: any;
  baseUrl = environment.ws_url;
  options: any;
  constructor(
    private http: HttpClient,
    private auth: AuthService) { }
  Headers() {
    const token = this.auth.getToken();
    this.options = {
      headers: new HttpHeaders({
        Accept: 'application/x-www-form-urlencoded',
        'Content-Type':  'application/json',
        Authorization: token
      })
    };

  }

  // HttpClient API get() method
  get(url: string): Observable<any> {
    this.Headers();
    return this.http.get<any>(this.baseUrl + url, this.options)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  // HttpClient API post() method
  post(url: string, parametre: any): Observable<any> {
    this.Headers();
    return this.http.post(this.baseUrl + url, parametre, this.options)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  // HttpClient API put() method
  put(url: string, parametre: any): Observable<any> {
    this.Headers();
    return this.http.put(this.baseUrl + url, JSON.stringify(parametre), this.options)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  // HttpClient API delete() method
  delete(url: string) {
    this.Headers();
    return this.http.delete(this.baseUrl + url, this.options)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  // Error handling
  handleError(error) {
     let errorMessage = '';
     if (error.error instanceof ErrorEvent) {
       // Get client-side error
       errorMessage = error.error.message;
     } else {
       // Get server-side error
       errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
     }
     window.alert(errorMessage);
     return throwError(errorMessage);
  }
}
