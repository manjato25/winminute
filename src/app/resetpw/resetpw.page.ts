import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import Swal from 'sweetalert2';
import { Location } from '@angular/common'

@Component({
  selector: 'app-resetpw',
  templateUrl: './resetpw.page.html',
  styleUrls: ['./resetpw.page.scss'],
})
export class ResetpwPage implements OnInit {
  resetForm: FormGroup
  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private location: Location
    ) {
    this.createformLogin();
   }
  createformLogin() {
    this.resetForm = this.formBuilder.group({
      email: ['', Validators.compose([
        Validators.required,
        Validators.email
      ])],

    });
  }
  resetPass() {
    const userData = {
      login: this.resetForm.get('email').value,
    };
    this.authService.resetpw(userData).subscribe((data:any) => {
      Swal.fire({
        text: data.message,
        showConfirmButton: false,
        icon: 'success',
        timer: 5000,
        showClass: {
          popup: 'animated fadeInDown faster'
        },
        hideClass: {
          popup: 'animated fadeOutUp faster'
        }
      });
      
      this.location.back();
    },
     err => {
      Swal.fire({
        text: err,
        showConfirmButton: false,
        icon: 'error',
        timer: 2000,
        showClass: {
          popup: 'animated fadeInDown faster'
        },
        hideClass: {
          popup: 'animated fadeOutUp faster'
        }
      });
    })
  }
  ngOnInit() {
  }

}
