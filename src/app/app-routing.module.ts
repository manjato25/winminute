import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './services/auth-guard.service'
const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'resetpw',
    loadChildren: () => import('./resetpw/resetpw.module').then(m => m.ResetpwPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule),  
  },
  {
    path: 'historique',
    loadChildren: () => import('./historique/historique.module').then( m => m.HistoriquePageModule)
  },
  {
    path: 'mon-profil',
    loadChildren: () => import('./mon-profil/mon-profil.module').then( m => m.MonProfilPageModule)
  },
  {
    path: 'modif-pw',
    loadChildren: () => import('./modif-pw/modif-pw.module').then( m => m.ModifPWPageModule)
  },
  {
    path: 'collectdetail/:idCollect',
    loadChildren: () => import('./collectdetail/collectdetail.module').then( m => m.CollectdetailPageModule)
  },
  {
    path: 'home/carte',
    loadChildren: () => import('./carte/carte.module').then( m => m.CartePageModule)
  },  
  {
    path: 'home/offre',
    loadChildren: () => import('./offre/offre.module').then( m => m.OffrePageModule)
  },
  {
    path: 'error-carte',
    loadChildren: () => import('./error-carte/error-carte.module').then( m => m.ErrorCartePageModule)
  },
  {
    path: 'carte-postal/:zipcode',
    loadChildren: () => import('./carte-postal/carte-postal.module').then( m => m.CartePostalPageModule)
  },
  {
    path: 'cartedefault',
    loadChildren: () => import('./cartedefault/cartedefault.module').then( m => m.CartedefaultPageModule)
  },
  {
    path: 'reglage',
    loadChildren: () => import('./reglage/reglage.module').then( m => m.ReglagePageModule)
  },
  {
    path: 'carte-detail/:idpdv',
    loadChildren: () => import('./carte-detail/carte-detail.module').then( m => m.CarteDetailPageModule)
  },
  {
    path: 'questionnaire/:campaign_id/:retailer_id',
    loadChildren: () => import('./questionnaire/questionnaire.module').then( m => m.QuestionnairePageModule)
  },  {
    path: 'statistique',
    loadChildren: () => import('./statistique/statistique.module').then( m => m.StatistiquePageModule)
  },
  {
    path: 'apropos',
    loadChildren: () => import('./apropos/apropos.module').then( m => m.AproposPageModule)
  },
  {
    path: 'faq',
    loadChildren: () => import('./faq/faq.module').then( m => m.FaqPageModule)
  },





];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
