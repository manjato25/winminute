import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { CollectService } from '../api/collect.service';
@Component({
  selector: 'app-modal-page',
  templateUrl: './modal-page.page.html',
  styleUrls: ['./modal-page.page.scss'],
})
export class ModalPagePage implements OnInit {
  dataSearch: any;
  valueData: any;
  dataAvalaible = false;

  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    private collectService: CollectService
  ) { }

  ngOnInit() {
    this.getdata();
  }
getdata() {
  this.valueData = this.navParams.data.value;
  if (this.collectService.getCollectfromDate() === undefined) {
   this.collectService.searchRetailler().subscribe((data: any) => {
      this.dataSearch = data;
    this.dataSearch = data.filter((item) => {
      return ((item.db7_store_name.toLowerCase().indexOf(this.valueData.toLowerCase()) > -1) || (item.db7_city.toLowerCase().indexOf(this.valueData.toLowerCase()) > -1) || (item.db7_full_address.toLowerCase().indexOf(this.valueData.toLowerCase()) > -1));
    });
    this.dataAvalaible = true;
    
  })
  } else {
    this.dataSearch = this.collectService.getCollectfromDate().filter((item) => {
      console.log(item);
     return ((item.retailer_name.toLowerCase().indexOf(this.valueData.toLowerCase()) > -1) || (item.city.toLowerCase().indexOf(this.valueData.toLowerCase()) > -1) || (item.address.toLowerCase().indexOf(this.valueData.toLowerCase()) > -1));
    });
    console.log(this.dataSearch);
    this.dataAvalaible = true;
  }

}
  async closeModal() {
    await this.modalController.dismiss();
  }
  async goToDetail(dataClose) {
    await this.modalController.dismiss(dataClose);
  }
}
