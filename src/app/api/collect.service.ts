import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HhtpService } from '../services/http.service';

@Injectable({
  providedIn: 'root'
})
export class CollectService {
  listCollect: any;

  constructor(
    private httpService : HhtpService) { }
  getCollect(collect): Observable<any> {
    return this.httpService.get('/collect/'+ collect);
  }
  getHistory(): Observable<any> {
    return this.httpService.get('collect/search?collect_date=');
  } 
   deleteCollect(id): Observable<any> {
    return this.httpService.delete('collect?id=' + id);
  }
  searchCollect(date): Observable<any> {
    return this.httpService.get('collect/search?collect_date=' + date);
  }
  searchRetailler(): Observable<any> {
    return this.httpService.get('retailer/search');
  }
  getIdCollect(id): Observable<any> {
    return this.httpService.get('collect/search?retailer_id=' + id);
  } 

  getCollectMap(longitude, latitude): Observable<any> {
    return this.httpService.get('campaign/search?longitude=' + longitude + '&latitude=' + latitude + '&campaign_ids=');
  }
  campaignZip(code): Observable<any> {
    return this.httpService.get('campaign/search_for_zips?zip=' + code);
  }
  setCollet(list) {
    this.listCollect = list;
  }
  getCollectfromDate() {
    return this.listCollect;
  }
}
