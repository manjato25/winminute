import { Injectable } from '@angular/core';
import { HhtpService } from '../services/http.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(
    private httpService : HhtpService
  ) { }
  
  profil(): Observable<any> {
    return this.httpService.get('user/profile');
  }
  editPw(pw): Observable<any> {
    return this.httpService.put('user/password', pw);
  }
  userHome(): Observable<any> {
    return this.httpService.get('user/home');
  }
  editUser(user): Observable<any> {
    return this.httpService.put('user/profile', user);
  }
}
