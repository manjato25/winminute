import { Injectable } from '@angular/core';
import { HhtpService } from '../services/http.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CampaignService {
  campaign: any;

  constructor(
    private httpService : HhtpService
  ) { }
  getCampaign() {
    return this.campaign;
  }
  setCampaign(campaign) {
    this.campaign = campaign;
  }
  camptaignQuestion(idCampaign, idRetailer): Observable<any> {
    return this.httpService.get('campaign/questionnaire?campaign_id=' + idCampaign +'&retailer_id=' + idRetailer);
  }
}
