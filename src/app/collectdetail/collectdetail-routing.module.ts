import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CollectdetailPage } from './collectdetail.page';

const routes: Routes = [
  {
    path: '',
    component: CollectdetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CollectdetailPageRoutingModule {}
