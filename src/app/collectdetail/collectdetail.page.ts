import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CollectService } from '../api/collect.service';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@Component({
  selector: 'app-collectdetail',
  templateUrl: './collectdetail.page.html',
  styleUrls: ['./collectdetail.page.scss'],
})
export class CollectdetailPage implements OnInit {
  dataDetail: any;
  date: any;
  dataAvalaible = false;
  questionRep: [string, unknown][];
  dataProposal: void;
  respName: any;
  constructor(
    private activatedRoute: ActivatedRoute,
    private collectService: CollectService,
    private photoViewer: PhotoViewer
  ) { 
    this.activatedRoute.params.subscribe((params: any) => {
      this.collectService.getCollect(params.idCollect).subscribe((data:any) => {
        console.log(data)
        this.dataDetail = data
        if (data.answer[60053] === undefined) {
          this.respName = ''
        } else {
          this.respName = data.answer[60053].reponse['0'].reponse
        }
       
       this.date = this.dataDetail.mission[params.idCollect].formatted_time;
     if (data.questions[646] !== undefined ) {
          this.questionRep = Object.entries(data.questions[646].questions)
          this.questionRep.forEach((el:any, index) => {
            el['question'] = el[0] + ' ' + el[1].label
            el['rep'] = el[1].reponse
            if (el[1].proposal !== null && el[1].proposal !== undefined) {
              el['proposal'] = el[1].proposal
            }
            el['type'] = el[1].type
          });
        } else if (data.questions[1099] !== undefined ) {
          this.questionRep = Object.entries(data.questions[1099].questions)
          this.questionRep.forEach((el:any) => {
            el['question'] = el[0] + ' ' + el[1].label
            el['rep'] = el[1].reponse
            if (el[1].proposal !== null && el[1].proposal !== undefined) {
              el['proposal'] = el[1].proposal
            }
            el['type'] = el[1].type
          });
          console.log(this.questionRep)
        } else if (data.questions[784] !== undefined ) {
          
          this.questionRep = Object.entries(data.questions[784].questions)
          this.questionRep.forEach((el:any) => {
            el['question'] = el[0] + ' ' + el[1].label
            el['rep'] = el[1].reponse
            if (el[1].proposal !== null && el[1].proposal !== undefined) {
              el['proposal'] = el[1].proposal
            }
            el['type'] = el[1].type
          });
         } else if (data.questions[785] !== undefined ) {
          
          this.questionRep = Object.entries(data.questions[785].questions)
          this.questionRep.forEach((el:any) => {
            el['question'] = el[0] + ' ' + el[1].label
            el['rep'] = el[1].reponse
            if (el[1].proposal !== null && el[1].proposal !== undefined) {
              el['proposal'] = el[1].proposal
            }
            el['type'] = el[1].type
          });
         }
       this.dataAvalaible = true;
      })
    });
  }
  seeImage(image) {
    this.photoViewer.show(image.toString());
  }
  ngOnInit() {
  }

}
