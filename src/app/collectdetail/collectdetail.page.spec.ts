import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CollectdetailPage } from './collectdetail.page';

describe('CollectdetailPage', () => {
  let component: CollectdetailPage;
  let fixture: ComponentFixture<CollectdetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollectdetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CollectdetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
