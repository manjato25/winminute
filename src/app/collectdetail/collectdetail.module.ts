import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CollectdetailPageRoutingModule } from './collectdetail-routing.module';

import { CollectdetailPage } from './collectdetail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CollectdetailPageRoutingModule
  ],
  declarations: [CollectdetailPage, ]
})
export class CollectdetailPageModule {}
