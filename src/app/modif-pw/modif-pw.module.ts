import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModifPWPageRoutingModule } from './modif-pw-routing.module';

import { ModifPWPage } from './modif-pw.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModifPWPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [ModifPWPage]
})
export class ModifPWPageModule {}
