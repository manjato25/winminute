import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModifPWPage } from './modif-pw.page';

const routes: Routes = [
  {
    path: '',
    component: ModifPWPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModifPWPageRoutingModule {}
