import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ModifPWPage } from './modif-pw.page';

describe('ModifPWPage', () => {
  let component: ModifPWPage;
  let fixture: ComponentFixture<ModifPWPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifPWPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ModifPWPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
