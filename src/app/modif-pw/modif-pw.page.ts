import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { UserService } from '../api/user.service';
import { Router } from '@angular/router';
import * as sha1 from 'sha1';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-modif-pw',
  templateUrl: './modif-pw.page.html',
  styleUrls: ['./modif-pw.page.scss'],
})
export class ModifPWPage implements OnInit {
  mdp1: any;
  img = 'assets/img/icon_app_warning.png'
  mdp2: any;
  mdp: any;
  formResetPass: FormGroup;
  constructor(
    private user: UserService,
    private router: Router,
    private formBuilder: FormBuilder,) {
      this.createformPassWord()
     }

  ngOnInit() {
  }
  createformPassWord() {
    this.formResetPass = this.formBuilder.group({
      mdp: ['', Validators.compose([
        Validators.required,
      ])],
      mdp1: ['', Validators.compose([
        Validators.required,
      ])],
      mdp2 : ['', Validators.compose([
        Validators.required,

      ])],

    });
  }
  modifierMDP() {
    if (this.formResetPass.get('mdp1').value !== this.formResetPass.get('mdp2').value) {
      Swal.fire({
        text: 'Mot de Passe incorrecte',
        showConfirmButton: false,
        icon: 'error',
        timer: 2000,
        showClass: {
          popup: 'animated fadeInDown faster'
        },
        hideClass: {
          popup: 'animated fadeOutUp faster'
        }
      });
    } else {
      const pass = {
        password: this.formResetPass.get('mdp1').value,
        old_password: this.formResetPass.get('mdp').value
      }
      this.user.editPw(pass).subscribe((resp: any) => {
        Swal.fire({
          text: resp.message,
          showConfirmButton: false,
          icon: 'success',
          timer: 2000,
          showClass: {
            popup: 'animated fadeInDown faster'
          },
          hideClass: {
            popup: 'animated fadeOutUp faster'
          }
        });
        this.router.navigate(['/home'])
      }, err => {
        Swal.fire({
          text: err,
          showConfirmButton: false,
          icon: 'error',
          timer: 2000,
          showClass: {
            popup: 'animated fadeInDown faster'
          },
          hideClass: {
            popup: 'animated fadeOutUp faster'
          }
        });
      })
    }

  }
}
