import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CarteDetailPage } from './carte-detail.page';

const routes: Routes = [
  {
    path: '',
    component: CarteDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CarteDetailPageRoutingModule {}
