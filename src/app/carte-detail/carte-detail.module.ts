import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CarteDetailPageRoutingModule } from './carte-detail-routing.module';

import { CarteDetailPage } from './carte-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CarteDetailPageRoutingModule
  ],
  declarations: [CarteDetailPage]
})
export class CarteDetailPageModule {}
