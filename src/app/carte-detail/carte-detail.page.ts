import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CampaignService } from '../api/campaign.service';

@Component({
  selector: 'app-carte-detail',
  templateUrl: './carte-detail.page.html',
  styleUrls: ['./carte-detail.page.scss'],
})
export class CarteDetailPage implements OnInit {
  idPDV: any;
  campaign: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private campaignService: CampaignService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: any) => {
      this.idPDV = params.idpdv
      this.campaign = this.campaignService.getCampaign();
      console.log(this.campaign)
        });
  }

}
