import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CartePostalPage } from './carte-postal.page';

const routes: Routes = [
  {
    path: '',
    component: CartePostalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CartePostalPageRoutingModule {}
