import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CartePostalPageRoutingModule } from './carte-postal-routing.module';

import { CartePostalPage } from './carte-postal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CartePostalPageRoutingModule
  ],
  declarations: [CartePostalPage]
})
export class CartePostalPageModule {}
