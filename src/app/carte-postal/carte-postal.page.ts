import { Component, OnInit, ElementRef, NgZone } from '@angular/core';
import { Map, latLng, tileLayer, Layer, marker } from 'leaflet';
import { ActivatedRoute, Router } from '@angular/router';
import { CollectService } from '../api/collect.service';
import { CampaignService } from '../api/campaign.service';
declare let L: any;
@Component({
  selector: 'app-carte-postal',
  templateUrl: './carte-postal.page.html',
  styleUrls: ['./carte-postal.page.scss'],
})
export class CartePostalPage {
  map: any;
  moiMarker: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private collectService: CollectService,
    private elementRef: ElementRef,
    public ngZone : NgZone,
    private campaignService: CampaignService,
    private router: Router
  ) { }
  leafletMap(latitude, longitude) {
    this.map = new Map('mapId2').setView([latitude, longitude], 12);
    tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
      attribution: 'winminutePro',
      maxZoom: 19
    }).addTo(this.map);
  }
  ionViewWillLeave() {
    this.map.off();
    this.map.remove();
    this.map = undefined
    document.getElementById('mapId2').innerHTML = "";
  }
  goToQuestion(property){
    this.elementRef.nativeElement.querySelector("#view_two")
    .addEventListener('click', (e : any)=> {
      this.ngZone.run(() => {
        this.campaignService.setCampaign(property)
       this.router.navigateByUrl('carte-detail/'+  property.id);
      })
    });
  }
  ionViewDidEnter() { 
    if (this.map != undefined) { this.map.remove(); }
    this.activatedRoute.params.subscribe((params: any) => {
      this.collectService.campaignZip(params.zipcode).subscribe((resp: any) => {
        this.leafletMap(resp.departments[0].latitude, resp.departments[0].longitude);
        var me = L.icon({
          iconUrl: 'assets/img/campagn.png',
        });
        this.map.on("click", e => {
          this.map.removeLayer( this.moiMarker)
          this.moiMarker = marker([e.latlng.lat, e.latlng.lng] , { icon: me }).addTo(this.map)
         });
         this.moiMarker = marker([resp.departments[0].latitude, resp.departments[0].longitude], { icon: me }).addTo(this.map)
        this.collectService.getCollectMap(resp.departments[0].longitude, resp.departments[0].latitude).subscribe((res: any) => {
          console.log(res);
          if (res.pdvs.length > 0) {
            for (const property of res.pdvs) {
              const fillColor = 'D01C1F';
              var image = 'data:image/svg+xml,';
              image += '%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%20standalone%3D%22no%22%3F%3E%0A%3Csvg%0A%20%20%20xmlns%3Adc%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Felements%2F1.1%2F%22%0A%20%20%20xmlns%3Acc%3D%22http%3A%2F%2Fcreativecommons.org%2Fns%23%22%0A%20%20%20xmlns%3Ardf%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2F02%2F22-rdf-syntax-ns%23%22%0A%20%20%20xmlns%3Asvg%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%20%20%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%20%20%20xmlns%3Asodipodi%3D%22http%3A%2F%2Fsodipodi.sourceforge.net%2FDTD%2Fsodipodi-0.dtd%22%0A%20%20%20xmlns%3Ainkscape%3D%22http%3A%2F%2Fwww.inkscape.org%2Fnamespaces%2Finkscape%22%0A%20%20%20width%3D%2238%22%0A%20%20%20height%3D%2238%22%0A%20%20%20viewBox%3D%220%200%2038%2038%22%0A%20%20%20version%3D%221.1%22%0A%20%20%20id%3D%22svg4614%22%0A%20%20%20sodipodi%3Adocname%3D%22svg.svg%22%0A%20%20%20inkscape%3Aversion%3D%220.92.0%20r15299%22%3E%0A%20%20%3Cpath%0A%20%20%20%20%20d%3D%22m%2036.43795%2C16.24046%20c%200%2C8.80707%20-17.286462%2C19.10825%20-17.286462%2C19.10825%200%2C0%20-17.8593306%2C-10.300183%20-17.8593306%2C-19.241902%200%2C-8.4829146%207.8672348%2C-14.2668557%2017.5740376%2C-14.2668557%209.704521%2C0%2017.571755%2C5.9175932%2017.571755%2C14.4005077%20z%22%0A%20%20%20%20%20id%3D%22path4610%22%0A%20%20%20%20%20inkscape%3Aconnector-curvature%3D%220%22%0A%20%20%20%20%20style%3D%22fill%3A%23' + fillColor + '%3Bstroke%3A%23cccccc%3Bstroke-width%3A2.66717052%22%20%2F%3E%0A%20%20%3Ctext%0A%20%20%20%20%20transform%3D%22translate(19%2018.5)%22%0A%20%20%20%20%20fill%3D%22%23fff%22%0A%20%20%20%20%20style%3D%22font-weight%3Abold%3Btext-align%3Acenter%3B%22%0A%20%20%20%20%20font-size%3D%2210%22%0A%20%20%20%20%20text-anchor%3D%22middle%22%0A%20%20%20%20%20id%3D%22text4612%22%3E';
              image += property.db3_campaign_type + '%3C%2Ftext%3E%0A%3C%2Fsvg%3E%0A';
              var myIcon = L.icon({
                iconUrl: image,
                iconSize: [32, 32],
                iconAnchor: [16, 32],
                popupAnchor:[0, -32]
              });
              const content =
              '<ion-card><ion-card-header><ion-card-subtitle>' + property.db7_full_address + '</ion-card-subtitle><ion-card-title>' + property.db7_store_name + '</ion-card-title></ion-card-header><ion-card-content><a id="view_two">'+ property.db3_campaign_name + '</a> <p> Distance: ' + property.distance + '</p> </ion-card-content></ion-card>';
              marker([property.db7_location_y, property.db7_location_x], { icon: myIcon }).addTo(this.map)
                .bindPopup(content)
                .on('popupopen' , () => {
                  this.goToQuestion(property);
                });
            }
          }
        })
      })
    })
  }

}
