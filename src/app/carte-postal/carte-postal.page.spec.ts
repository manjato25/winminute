import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CartePostalPage } from './carte-postal.page';

describe('CartePostalPage', () => {
  let component: CartePostalPage;
  let fixture: ComponentFixture<CartePostalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartePostalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CartePostalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
